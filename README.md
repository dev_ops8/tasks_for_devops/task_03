## Task_03
### Description of the task
Create auto generation cert for our site
#### Check-points:
-  Site available on DNS
-  Create docker-compose, that will be generate certs for domain name 
-  Create CI pipe, that will add domain name in our environment
### Result of job
- 1 step: Create docker-compose, that generate certs for domain
- 2 step: Environment with our domain