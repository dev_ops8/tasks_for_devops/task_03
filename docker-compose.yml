version: '3.0'

services:
# Mysql setting
  mysql:
    image: mysql:8
    container_name: react-springboot_mysql
    env_file:
      - .env
    volumes:
      - mysql_data:/var/lib/mysql:rw 
    labels:
      - traefik.enable=false
    healthcheck:
      test: "mysqladmin ping -h localhost -u $${MYSQL_USER} --password=$${MYSQL_PASSWORD}"
      interval: 10s
      timeout: 5s
      retries: 5
      start_period: 15s
    networks:
      - backend
    restart: unless-stopped
# Backend setting
  backend:
    depends_on:
      mysql:
        condition: service_healthy
    image: registry.gitlab.com/dev_ops8/tasks_for_devops/task_03/task_03-backend:latest
    deploy:
      replicas: 2
      resources:
        limits:
          memory: 300M
    build:
      context: ./backend
      dockerfile: Dockerfile
    env_file:
      - .env 
    labels:
      - traefik.enable=false
    healthcheck:
      test: curl --fail http://localhost:8080/actuator/health/ || exit 1
      interval: 10s
      timeout: 5s
      retries: 3
      start_period: 20s
    networks:
      - backend
    restart: unless-stopped
# Frontend settings
  frontend:
    depends_on:
      - backend
    image: registry.gitlab.com/dev_ops8/tasks_for_devops/task_03/task_03-frontend:latest
    deploy:
      replicas: 3
      resources:
        limits:
          memory: 300M
    build:
      context: ./frontend
      dockerfile: Dockerfile
    env_file:
      - .env  
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.frontend.rule=Host(`domain.com`)"
    healthcheck:
      test: curl --fail http://localhost:80 || exit 1
      interval: 10s
      timeout: 5s
      retries: 3
      start_period: 20s
    networks:
      - backend
      - frontend
    restart: unless-stopped
# Traefik setting
  traefik:
    depends_on:
      - frontend
    image: traefik:v2.2
    container_name: react-springboot_traefik
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.traefik.entrypoints=https"
      - "traefik.http.routers.traefik.rule=Host(`traefik.domain.com`)"
      - "traefik.http.routers.traefik.tls=true"
      - "traefik.http.routers.traefik.tls.certresolver=letsEncrypt"
      - "traefik.http.routers.traefik.service=api@internal"
      - "traefik.http.services.traefik-traefik.loadbalancer.server.port=888"
      - "traefik.http.middlewares.traefik-auth.basicauth.users=admin:$$apr1$$vDSqkf.v$$GTJOtsd9CBiAFFnHTI2Ds1"
      - "traefik.http.routers.traefik.middlewares=traefik-auth"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./traefik/traefik.yml:/traefik.yml:ro 
    ports:
      - "8081:80"
      - "443:443"
    networks:
      - frontend
    restart: unless-stopped
# Networks
networks:
  frontend:
    driver: bridge
    name: frontend
  backend:
    driver: bridge
    name: backend
# Named volumes
volumes:
  mysql_data:
    name: mysql_data